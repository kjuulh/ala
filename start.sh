#!/bin/sh

docker run --detach -p 8888:8888 -e JUPYTER_ENABLE_LAB=yes -v /home/hermansen/Documents/git:/home/jovyan/work --name docker-jupyter-lab jupyter/datascience-notebook:9b06df75e445 start.sh jupyter lab --LabApp.token='' --LabApp.password=''

sleep 1s

chromium http://localhost:8888
